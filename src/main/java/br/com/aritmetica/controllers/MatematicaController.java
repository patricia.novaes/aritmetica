package br.com.aritmetica.controllers;


import br.com.aritmetica.dto.EntradaDTO;
import br.com.aritmetica.dto.RespostaDTO;
import br.com.aritmetica.services.MatematicaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/matematica")
public class MatematicaController {

    //http://localhost:8080/matematica/olaMundo
//    @GetMapping("/olaMundo")
//    public String olaMundo(){
//        return "Heloooooooo!";
//    }

//    @PutMapping
//    public String olaPutin(){
//        return "Chamando método put";
//    }

    @Autowired
    private MatematicaService service;

    @PutMapping("/soma")
    public RespostaDTO soma(@RequestBody EntradaDTO entrada){
        if(!numeroNatural(entrada)){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Atenção envie apenas números positivos");
        } else if(entrada.getNumeros().size() <= 1 ){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Envie pelo menos 2 numeros para que a soma seja efetuada!");
        }

        RespostaDTO resposta = service.soma(entrada);
        return resposta;
    }

    @PutMapping("/subtracao")
    public RespostaDTO subtracao(@RequestBody EntradaDTO entrada){
        if(!numeroNatural(entrada)){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Atenção envie apenas números positivos");
        } else if(entrada.getNumeros().size() <= 1 ){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Envie pelo menos 2 numeros para que a subtração seja efetuada!");
        }

        RespostaDTO resposta = service.subtracao(entrada);
        return resposta;
    }


    @PutMapping("/multiplicacao")
    public RespostaDTO multiplicacao(@RequestBody EntradaDTO entrada){
        if(!numeroNatural(entrada)){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Atenção envie apenas números positivos");
        } else if(entrada.getNumeros().size() <= 1 ){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Envie pelo menos 2 numeros para que a soma seja efetuada!");
        }

        RespostaDTO resposta = service.multiplicacao(entrada);
        return resposta;
    }

    //divisão
    @PutMapping("/divisao")
    public RespostaDTO divisao(@RequestBody EntradaDTO entrada){
        if(!numeroNatural(entrada)){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Atenção envie apenas números positivos");
        } else if(!podeDividir(entrada)){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Atenção envie o primeiro número maior que o segundo para que a divisão seja efetuada");
        } else if(entrada.getNumeros().size() <= 1 ){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Envie pelo menos 2 numeros para que a soma seja efetuada!");
        }

        RespostaDTO resposta = service.divisao(entrada);
        return resposta;
    }

    private boolean numeroNatural(EntradaDTO entrada){
        boolean retorno = true;
        for (int n: entrada.getNumeros()){
            if(n < 0){
                retorno = false;
            }
        }
        return retorno;
    }

    private boolean podeDividir(EntradaDTO entrada){
        boolean retorno = true;
        for (int i = 0; i < entrada.getNumeros().size(); i++) {
            if(entrada.getNumeros().get(i) < entrada.getNumeros().get(i++)){
                retorno = false;
            }
        }
        return retorno;
    }

}
