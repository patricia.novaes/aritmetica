package br.com.aritmetica;

import br.com.aritmetica.controllers.MatematicaController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AritmeticaApplication {

	public static void main(String[] args) {
		SpringApplication.run(AritmeticaApplication.class, args);
	}

}
