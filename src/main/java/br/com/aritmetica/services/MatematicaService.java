package br.com.aritmetica.services;

import br.com.aritmetica.dto.EntradaDTO;
import br.com.aritmetica.dto.RespostaDTO;
import org.springframework.stereotype.Service;

@Service
public class MatematicaService {

    public RespostaDTO soma(EntradaDTO entrada){
        int numero = 0;
        for (int n: entrada.getNumeros()) {
            numero += n;
        }
        RespostaDTO resposta= new RespostaDTO();
        resposta.setResultado(numero);
        return resposta;
    }

    public RespostaDTO subtracao(EntradaDTO entrada) {
        int numero = 0;

        for(int i = 0; i < entrada.getNumeros().size(); i++) {
            int sub = entrada.getNumeros().get(i);
            for (int j = i + 1; j < entrada.getNumeros().size(); j++) {
                sub -= entrada.getNumeros().get(j);
                numero = sub;
            }
        }
        RespostaDTO resposta= new RespostaDTO();
        resposta.setResultado(numero);
        return resposta;
    }

    public RespostaDTO multiplicacao(EntradaDTO entrada) {
        int numero = entrada.getNumeros().get(0);
        for (int i = 1; i < entrada.getNumeros().size(); i++) {
            numero *= entrada.getNumeros().get(i);
        }
        RespostaDTO resposta= new RespostaDTO();
        resposta.setResultado(numero);
        return resposta;
    }

    public RespostaDTO divisao(EntradaDTO entrada) {
        int numero = entrada.getNumeros().get(0);
        for (int i = 1; i < entrada.getNumeros().size(); i++) {
            numero /= entrada.getNumeros().get(i);
        }
        RespostaDTO resposta= new RespostaDTO();
        resposta.setResultado(numero);
        return resposta;
    }
}
